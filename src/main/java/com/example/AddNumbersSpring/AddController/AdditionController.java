package com.example.AddNumbersSpring.AddController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.AddNumbersSpring.domain.Addtion;

@Controller
	public class AdditionController 
	{
		@GetMapping("/index")
		public String ViewForm(Model model)
		{
			model.addAttribute("Addition", new Addtion());
			return "index";
		}
		
	@PostMapping("/addtion")
		public String addForm(@ModelAttribute Addtion addtion,BindingResult result,Model model)
		{
		// System.out.println("Addition object recieved" +addtion.getNum1() +"num2 " +addtion.getNum2());
			model.addAttribute("addtion",addtion);
			return "calculate";
		}
	
	}
	
